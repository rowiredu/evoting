import { imgPathWithTransforms, imgSrcSetFull } from '../../utils/image';
import { DPR_1, SLIDER_GRID_LRG, SLIDER_GRID_MED, SLIDER_GRID_SML, SLIDER_GRID_XSML } from '../../utils/constants';

const GRID_LRG_WIDTH = 255;
const GRID_MED_WIDTH = 305;
const GRID_SML_WIDTH = 360;
const GRID_XSML_WIDTH = 550;

const LOGO_DESIGN = '055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png';
const WORDPRESS = 'ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png';
const VOICE_OVER = '055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png';
const ANIMATED_EXPLAINER = '055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png';
const SOCIAL = '055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png';
const SEO = '055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png';
const ILLUSTRATION = '055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png';
const TRANSLATION = '055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png';
const DATA_ENTRY = '055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png';
const BOOK_COVERS = '055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png';

const getTouchSrcSet = (filename) => imgSrcSetFull({ width: GRID_LRG_WIDTH, img: filename });
const getTouchSrc = (filename) => imgPathWithTransforms({ width: GRID_LRG_WIDTH, dpr: DPR_1, img: filename });
const getSrc = (filename) => imgPathWithTransforms({ width: GRID_XSML_WIDTH, dpr: DPR_1, img: filename });

const getPictureData = (filename) => [
    {
        srcSet: imgSrcSetFull({ width: GRID_LRG_WIDTH, img: filename }),
        media: SLIDER_GRID_LRG
    },
    {
        srcSet: imgSrcSetFull({ width: GRID_MED_WIDTH, img: filename }),
        media: SLIDER_GRID_MED
    },
    {
        srcSet: imgSrcSetFull({ width: GRID_SML_WIDTH, img: filename }),
        media: SLIDER_GRID_SML
    },
    {
        srcSet: imgSrcSetFull({ width: GRID_XSML_WIDTH, img: filename }),
        media: SLIDER_GRID_XSML
    }
];

export const featureHome = [
    {
        name: 'creative_logo_design',
        subCategoryId: 49,
        categorySlug: 'graphics-design',
        subCategorySlug: 'creative-logo-design',
        image: {
            src: getSrc(LOGO_DESIGN),
            touchSrcSet: getTouchSrcSet(LOGO_DESIGN),
            touchSrc: getTouchSrc(LOGO_DESIGN)
        },
        picture: getPictureData(LOGO_DESIGN)
    },
    {
        name: 'wordpress_services',
        subCategoryId: 91,
        categorySlug: 'programming-tech',
        subCategorySlug: 'wordpress-services',
        image: {
            src: getSrc(WORDPRESS),
            touchSrcSet: getTouchSrcSet(WORDPRESS),
            touchSrc: getTouchSrc(WORDPRESS)
        },
        picture: getPictureData(WORDPRESS)
    },
    {
        name: 'voice_overs',
        subCategoryId: 13,
        categorySlug: 'music-audio',
        subCategorySlug: 'voice-overs',
        image: {
            src: getSrc(VOICE_OVER),
            touchSrcSet: getTouchSrcSet(VOICE_OVER),
            touchSrc: getTouchSrc(VOICE_OVER)
        },
        picture: getPictureData(VOICE_OVER)
    },
    {
        name: 'animated_explainer',
        subCategoryId: 228,
        categorySlug: 'video-animation',
        subCategorySlug: 'whiteboard-explainer-videos',
        image: {
            src: getSrc(ANIMATED_EXPLAINER),
            touchSrcSet: getTouchSrcSet(ANIMATED_EXPLAINER),
            touchSrc: getTouchSrc(ANIMATED_EXPLAINER)
        },
        picture: getPictureData(ANIMATED_EXPLAINER)
    },
    {
        name: 'social_media',
        subCategoryId: 67,
        categorySlug: 'online-marketing',
        subCategorySlug: 'social-marketing',
        image: {
            src: getSrc(SOCIAL),
            touchSrcSet: getTouchSrcSet(SOCIAL),
            touchSrc: getTouchSrc(SOCIAL)
        },
        picture: getPictureData(SOCIAL)
    },
    {
        name: 'seo_services',
        subCategoryId: 65,
        categorySlug: 'online-marketing',
        subCategorySlug: 'seo-services',
        image: {
            src: getSrc(SEO),
            touchSrcSet: getTouchSrcSet(SEO),
            touchSrc: getTouchSrc(SEO)
        },
        picture: getPictureData(SEO)
    },
    {
        name: 'illustration',
        subCategoryId: 50,
        categorySlug: 'graphics-design',
        subCategorySlug: 'digital-illustration',
        image: {
            src: getSrc(ILLUSTRATION),
            touchSrcSet: getTouchSrcSet(ILLUSTRATION),
            touchSrc: getTouchSrc(ILLUSTRATION)
        },
        picture: getPictureData(ILLUSTRATION)
    },
    {
        name: 'translation',
        subCategoryId: 108,
        categorySlug: 'writing-translation',
        subCategorySlug: 'quality-translation-services',
        image: {
            src: getSrc(TRANSLATION),
            touchSrcSet: getTouchSrcSet(TRANSLATION),
            touchSrc: getTouchSrc(TRANSLATION)
        },
        picture: getPictureData(TRANSLATION)
    },
    {
        name: 'data_entry',
        subCategoryId: 339,
        categorySlug: 'business',
        subCategorySlug: 'data-entry',
        image: {
            src: getSrc(DATA_ENTRY),
            touchSrcSet: getTouchSrcSet(DATA_ENTRY),
            touchSrc: getTouchSrc(DATA_ENTRY)
        },
        picture: getPictureData(DATA_ENTRY)
    },
    {
        name: 'book_covers',
        subCategoryId: 51,
        nestedSubCategoryId: 2318,
        categorySlug: 'graphics-design',
        subCategorySlug: 'book-design',
        nestedSubCategorySlug: 'cover',
        image: {
            src: getSrc(BOOK_COVERS),
            touchSrcSet: getTouchSrcSet(BOOK_COVERS),
            touchSrc: getTouchSrc(BOOK_COVERS)
        },
        picture: getPictureData(BOOK_COVERS)
    }
];
