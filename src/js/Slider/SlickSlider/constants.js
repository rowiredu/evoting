const ITEM_GRID_XLRG = 1239;
const ITEM_GRID_LRG = 1059;
const ITEM_GRID_MED = 799;
const ITEM_GRID_SML = 599;

export const SLIDER_BREAKPOINTS = [
    {
        breakpoint: ITEM_GRID_XLRG,
        slides: 4
    },
    {
        breakpoint: ITEM_GRID_LRG,
        slides: 3
    },
    {
        breakpoint: ITEM_GRID_MED,
        slides: 2
    },
    {
        breakpoint: ITEM_GRID_SML,
        slides: 1
    }
];

export const SLIDES_TO_SHOW = 5;
export const INITIAL_SLIDE = 0;
export const AUTOPLAY_SPEED = 3000;

export const PREV = 'prev';
export const NEXT = 'next';
