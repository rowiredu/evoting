import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Otp from '@/components/Otp'
import Voting from '@/components/Voting'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/otp',
      name: 'Otp',
      component: Otp
    },
    {
      path: '/voting',
      name: 'Voting',
      component: Voting
    }
  ]
})
