import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pVote: false,
    vpVote: false,
    gsVote: false,
    fsVote: false,
    prVote: false,
    goVote: false,
    csVote: false,
    sfeVote: false,
    paiVote: false,
    cpVote: false,
    upVote: false,
    currentTab: "Presidential",
    OTP: ''
  },
  
  getters: {
    // Here we will create a getter
  },
  
  mutations: {
    setOTP(state, payload) {
      state.OTP = payload
    },
    changeTab(state, payload) {
      state.currentTab = payload
    },
    changep(state, payload) {
        state.pVote = true
        state.currentTab = "VicePresident"
    },
    changevp(state, payload) {
      state.vpVote = true
      state.currentTab = "GeneralSecretary"
    },
    changegs(state, payload) {
    state.gsVote = true
    state.currentTab = "FinancialSecretary"
    },
    changefs(state, payload) {
      state.fsVote = true
      state.currentTab = "PublicRelations"
    },
    changepr(state, payload) {
      state.prVote = true
      state.currentTab = "GeneralOrganizer"
    },
    changego(state, payload) {
      state.goVote = true
      state.currentTab = "CordinatingSecretary"
    },
    changecs(state, payload) {
      state.csVote = true
      state.currentTab = "SecretaryForEducation"
    },
    changesfe(state, payload) {
      state.sfeVote = true
      state.currentTab = "PressAndInformation"
    },
    changepai(state, payload) {
      state.paiVote = true
      state.currentTab = "ComputerPrefect"
    },
    changecp(state, payload) {
      state.cpVote = true
      state.currentTab = "UtilityPrefect"
    },
    changeup(state, payload) {
      state.upVote = true
      state.currentTab = "Presidential"
    },
  },
  
  actions: {
    // Here we will create Larry
  }
});